---
title: "Extra: Qualitätssicherung"
subtitle: "Einführung in die moderne statistische Datenanalyse mit R"
author: "Fabian Mundt"
date: 14.01.2025
output: 
    tufte::tufte_html:
        highlight: pygments
        tufte_variant: envisioned
editor_options: 
  chunk_output_type: console
---

```{r setup, echo=FALSE, warning=FALSE, message=FALSE}
# Pakete
library(tidyverse)
library(tufte)

# Globale knitr Optionen
knitr::opts_chunk$set(comment = "#>")
```

Die Syntax von R ist vergleichsweise einfach zu erlernen und 
bietet viele Freiheiten in der Gestaltung des Codes. 
Hierin liegt zugleich eine gewisse Problematik, 
da schnell unübersichtliche Skripte entstehen. 

Aus diesem Grund bietet es sich an, 
sich so streng wie möglich an Richtlinien zu halten.
Darüber hinaus exitieren einige Helfer, 
die den Code (semi-)automatisch nach eingestellten Regeln formatieren.

## Allgemeine Hinweise

- Ein typisches Skript sollte in kleinere "Chunks" unterteilt werden, die durch _Kommentarblöcke_ voneinander abgegrenzt sind.
- Alle _Abhängigkeiten_ sollten am Anfang des Skripts in einem eigenen Block gesammelt werden. Spätere `library()` Aufrufe sind zu vermeiden.
Für größere Projekte bietet es sich ggf. an ein gesondertes Skript für die verwendeten Pakete zu nutzen.
- Es sollte eine möglichst einheitliche _Namensgebung_ (für Dateien, Variablen, Funktionen) stattfinden. Bewährt haben sich kurze, selbsterklärende Namen, die durch einen Unterstrich unterteilt sind.
- Das Skript sollte in de _Länge_, nicht die Breite wachsen. Es empfiehlt sich spätestens nach 100 Zeichen einen Umbruch zu machen.
- Auch sollten Skripte nur so komplex wie nötig sein: _weniger ist mehr_.
- _Code-Wiederholungen_ sollten vermieden werden. Sobald man mehrfach einen Code-Abschnitt per Copy & Paste dupliziert, ist eine entsprechende Funktion i. d. R. sinnvoller.
- Im Sinne der Reproduzierbarkeit ist es empfehlenswert die _Arbeitsumgebung nicht zu speichern_.
- Außerdem ist es sinnvoll _R-Objekte_, die in anderen Zusammenhängen erneut verwendet werden, zu speichern.
- Dieses Vorgehen ermöglicht das Verwenden _vieler, relativ kurzer Skripte_, 
die je eine Aufgabe erfüllen. Es gilt, besser viele kurze Dateien als eine unübersichtliche, lange.

## Beispiele

```{r}
# Namensgebung
# Schlecht
EinDatensatz <- data.frame(x = 1:30, y = rep(TRUE, 30))
functionToCalculateMean <- function(x) sum(x) / length(x)
# Gut
df_test <- data.frame(x = 1:30, y = rep(TRUE, 30))
mean <- function (x) sum(x) / length(x)

# Aufräumen
rm(EinDatensatz)
rm(df_test)
rm(functionToCalculateMean)
rm(mean)
```

## Automatisierung: `styler`

```{r}
ugly_code <- "a=function( x){1+1}           "

styler::style_text(ugly_code)

# prettycode ermöglicht das Einfärben des neu formatierten Codes in der R-Konsole.
```


Von `style_text()` existieren vier Varianten:

- `style_file()` styled .R und/oder .Rmd Datein.
- `style_dir()` styled alle .R und/oder .Rmd Dateien in einem Verzeichnis.
- `style_pkg()` styled alle Quelldateien eins Pakets.
- _RStudio Addins_ styled die aktuell offene Datei, das aktuell offene Paket oder eine ausgewählte Code-Region.

_Die einzelnen Styleregeln können nach den eigenen Präferenzen angepasst werden (siehe folgender Abschnitt)._ 

```{r}
styler::style_text(
  "data_frame(
     small  = 2 ,
     medium = 4,#comment without space
     large  = 6
   )", strict = FALSE
)
```

### Konfigurationen

- `usethis::use_tidy_style()` styled das projekt nach dem tidyverse Styleguide.
- https://lorenzwalthert.netlify.com/post/customizing-styler-the-quick-way/
- https://styler.r-lib.org/articles/customizing_styler.html

## Tools für die Fehlerminimierung

Zur Fehlerminimierung und -behebung sind vor allem drei Werkzeuge relevant:

- Basistools (traceback(), warnings())
- RStudio live Feedback
- Statische Code-Analyse (`linter`)

Im Folgenden wird das `linter` Paket thematisiert, 
das eine statische Analyse von sowohl Style- und Syntaxproblemen als auch semantischen Problemen ermöglicht.
Im Unterschied zu `styler` geschieht das Beheben der Probleme _nicht automatisch_. 
Es wird nichts am Dokument verändert.

### Verfügbare linter

Quelle: https://github.com/jimhester/lintr/blob/master/README.md

- `Syntax errors`: reported by [parse](https://www.rdocumentation.org/packages/base/versions/3.4.0/topics/parse).
- `object_usage_linter`: check that closures have the proper usage using
  [codetools::checkUsage()](https://www.rdocumentation.org/packages/codetools/versions/0.2-15/topics/checkUsage).  Note this runs
  [base::eval()](https://www.rdocumentation.org/packages/base/versions/3.4.0/topics/eval) on the code, so do not use with untrusted code.
- `absolute_path_linter`: check that no absolute paths are used (e.g. "/var", "C:\\System", "~/docs").
- `nonportable_path_linter`: check that file.path() is used to construct safe and portable paths.
- `pipe_continuation_linter`: Check that each step in a pipeline is on a new
  line, or the entire pipe fits on one line.
- `assignment_linter`: check that `<-` is always used for assignment
- `camel_case_linter`: check that objects are not in camelCase.
- `closed_curly_linter`: check that closed curly braces should always be on their
  own line unless they follow an else.
- `commas_linter`: check that all commas are followed by spaces, but do not
  have spaces before them.
- `commented_code_linter`: check that there is no commented code outside of roxygen comments.
- `cyclocomp_linter`: check for overly complicated expressions.
- `equals_na_linter`: check for x == NA
- `extraction_operator_linter`: check that the `[[` operator is used when extracting a single
  element from an object, not `[` (subsetting) nor `$` (interactive use).
- `function_left_parentheses_linter`: check that all left parentheses in a
  function call do not have spaces before them.
- `implicit_integer_linter`: check that integers are explicitly typed using the form `1L` instead of `1`.
- `infix_spaces_linter`: check that all infix operators have spaces around them.
- `line_length_linter`: check the line length of both comments and code is less than
  length.
- `no_tab_linter`: check that only spaces are used, never tabs.
- `object_length_linter`: check that function and variable names are not more than `length` characters.
- `object_name_linter`: check that object names conform to a single naming
  style, e.g. CamelCase, camelCase, snake_case, dotted.case, lowercase,
  or UPPERCASE.
- `open_curly_linter`: check that opening curly braces are never on their own
  line and are always followed by a newline.
- `paren_brace_linter`: check that there is a space between right parenthesis and an opening curly brace.
- `semicolon_terminator_linter`: check that no semicolons terminate statements.
- `seq_linter`: check for `1:length(...)`, `1:nrow(...)`, `1:ncol(...)`,
  `1:NROW(...)`, and `1:NCOL(...)` expressions. These often cause bugs when the
  right hand side is zero. It is safer to use `seq_len()` or `seq_along()`
  instead.
- `single_quotes_linter`: check that only single quotes are used to delimit
  string constants.
- `spaces_inside_linter`: check that parentheses and square brackets do not have
  spaces directly inside them.
- `spaces_left_parentheses_linter`: check that all left parentheses have a space before them
  unless they are in a function call.
- `todo_comment_linter`: check that the source contains no TODO comments (case-insensitive).
- `trailing_blank_lines_linter`: check there are no trailing blank lines.
- `trailing_whitespace_linter`: check there are no trailing whitespace characters.
- `T_and_F_symbol_linter`: avoid the symbols `T` and `F` (for `TRUE` and `FALSE`).
- `undesirable_function_linter`: report the use of undesirable functions, e.g. `options` or `sapply` and suggest an alternative.
- `undesirable_operator_linter`: report the use of undesirable operators, e.g. `:::` or `<<-` and
  suggest an alternative.
- `unneeded_concatenation_linter`: check that the `c` function is not used without arguments nor
  with a single constant.

## Konfiguration

Jedes Projekt kann 
-- muss aber nicht -- 
eine eigene Konfigurationsdatei enthalten. 
Diese Datei (Standardname: _.lintr_) muss im Debian Control Field Format sein. 
Folgende Aspekte können eingestellt werden 
(siehe https://github.com/jimhester/lintr/blob/master/README.md):

- linters - see ?with_defaults for example of specifying only a few non-default linters.
- exclusions - a list of filenames to exclude from linting. You can use a named item to exclude only certain lines from a file.
- exclude - a regex pattern for lines to exclude from linting. Default is "# nolint"
- exclude_start - a regex pattern to start exclusion range. Default is "# nolint start"
- exclude_end - a regex pattern to end exclusion range. Default is "# nolint end"

An example file that uses 120 character line lengths, excludes a couple of files and sets different default exclude regexs follows.

```
linters: with_defaults(line_length_linter(120))
exclusions: list("inst/doc/creating_linters.R" = 1, "inst/example/bad.R", "tests/testthat/exclusions-test")
exclude: "# Exclude Linting"
exclude_start: "# Begin Exclude Linting"
exclude_end: "# End Exclude Linting"
```

## Integration in RStudio

Nach der Installation des `linter` Pakets kann die statische Code-Analyse per _RStudio Addin_ ausgeführt werden. 
Die Ergebnisse der Analyse werden im "Markers"-Fenster angezeigt. 
Dieses muss in den Einstellungen aktiviert werden: 
Menu "Tools" -> "Global Options..." -> "Options" -> "Code" -> "Diagnostics" -> check "Show diagnostics for R".
