---
title: "Datensicherung"
subtitle: "Einführung in die moderne statistische Datenanalyse mit R"
author: "Fabian Mundt"
date: 15.01.2025
output: 
    tufte::tufte_html:
        highlight: pygments
        tufte_variant: envisioned
editor_options: 
  chunk_output_type: console
---

```{r setup, echo=FALSE, warning=FALSE, message=FALSE}
# Pakete
library(tidyverse)
library(haven)
library(tufte)

# Globale knitr Optionen
knitr::opts_chunk$set(comment = "#>")
```

Sobald die Daten in R importiert sind, stellen sich neue Porbleme.
Insbesondere die Frage, wie mit den Daten sinnvoll weiter gearbeitet werden kann, ist dabei relevant. Es ist wenig sinvoll die Daten mit jedem Ausführen der Analysen neu einzulesen. Das bedeutet nicht, dass ein sauberes Importskript unnütz ist, sondern lediglich, dass über den Austausch von R Objekten über Skripte (und Computer) hinweg nachgedacht werden muss.

Hier wird das Ziel Verfolgt, ein eigenes kleines Beispielprojekt anzulegen, dass _best practice_ Strukturen umsetzt und als Blaupause für künftige Projekte dienen kann.

## Daten für die Projektarbeit speichern

```{marginfigure}
Die Unterschiede zwischen `save()` und `saveRDS()` sind Thema vieler Artikel, 
u. a. https://www.fromthebottomoftheheap.net/2012/04/01/saving-and-loading-r-objects/
````

`write_rds()` und `read_rds()` sind konsistente Wrapper um `saveRDS()` und `readRDS()`. 
`write_rds()` komprimiert nicht standardmäßig, 
da der Speicherplatz im Allgemeinen günstiger ist als die zur 
Verfügung stehende Zeit.

Sowohl für die produktive Arbeit allein als auch in Teams ist es sinnvoll, 
häufig verwendete R-Objekte (Datensätze, Plots, Ergebnisse ...) in RDS-Dateien zu speichern. 
Dies erleichtert und beschleunigt die Reproduzierbarkeit. 
Es wird empfohlen, 
alle RDS-Dateien in einem speziellen Unterverzeichnis des Projekts zu speichern, 
z.B. "repository".  

```{r, eval=FALSE}
# Verzeichnis erzeugen, falls es nicht existiert.
dir.create('repository', showWarnings = FALSE)

df_pisa <- read_csv("data/teacher-pisa.csv")

write_rds(df_pisa, "repository/df-pisa.rds")

df_pisa_new <- read_rds("data/df-pisa.rds")
```

## Sinnvolle Strukturen schaffen

- Ein Verzeichnis für die Rohdaten.
- Ein Importskript.
- Ein Verzeichnis für gespeicherte R-Objekte (RDS-Dateien).
- Analyseskripte je nach Fragestellung.
- …

## Ergänzender Hinweis: Git für die komfortable Teamarbeit und sichere Einzelarbeit

![](https://cdn.dribbble.com/users/511295/screenshots/2629099/octocat-wave-dribbble.gif)
