# Ich will PISA Daten aufbereiten


# Pakete laden ----------------------------------------------------------------------------------------------------

library(readr)
library(dplyr)

# Wenn ihr mit dem Tidyverse arbeiten wollt und keine Lust habt die einzelnen Pakete zu lernen,
# dann genügt es dieses "Metapaket" zu laden:

library(tidyverse)
library(haven)

# Daten laden -----------------------------------------------------------------------------------------------------

df_pisa_full_teacher_raw <- 
  read_rds(file = "repository/df-pisa-full-raw.rds")

# Daten aufbereiten -----------------------------------------------------------------------------------------------

# Spalten umbenenenn -----

df_pisa_full_teacher_raw

df_pisa_full_teacher_raw |> 
  rename(
    "Land Name" = CNTRYID,
    "Land" = CNT
  ) |> 
  rename(
    ID = CNTSCHID,
    land_name = `Land Name`
  ) |>
  rename(
    "Lehrer ID" = TEACHERID
  )

df_pisa_full_teacher_raw |> 
  rename(
    "Land Name" = CNTRYID,
    "Land" = CNT,
    ID = CNTSCHID,
    "Lehrer ID" = TEACHERID
  )

# Base R Ansatz, um Spaltennamen anzuzeigen (es ist noch etwas mehr zu tun, um diese zu verändern):
#
# df_pisa_full_teacher_raw |> 
#   colnames()

# Spalten auswählen ----

df_pisa_full_teacher_raw |> 
  rename(
    land_name = CNTRYID,
    land = CNT
  ) |> 
  select(
    land_name,
    land
  )

df_pisa_full_teacher_raw |> 
  select(
    land_name = CNTRYID,
    land = CNT
  )

df_pisa_full_teacher_raw |> 
  select(
    -CNTRYID,
    -CNT
  )

# Pipes nutzen (im Kontext des Speicherns von Objekten)

df_pisa_land_name_land_id <- 
  df_pisa_full_teacher_raw |> 
  rename(
    land_name = CNTRYID,
    land = CNT
  ) 

df_pisa_land_name_land_id |> 
  select(
    land_name,
    land
  )

df_pisa_land_name_land_id_only <- 
  df_pisa_land_name_land_id |> 
  select(
    land_name,
    land
  )

# Wir wollen nun die SPSS labels nutzen:
# Spezielle Situation, da es auch as_factor() aus dem forcats Paket gibt.
# Deshalb müssen wir hier ganz genau sagen, welche as_factor() Funktion wir haben wollen.
# 
# forcats::as_factor()
# haven::as_factor()

df_pisa_land_name_land_id_only |> 
  haven::as_factor()

# Spalten verändern ----

# Kopieren von Spalten

df_pisa_land_name_land_id |> 
  mutate(
    land_new = land
  ) |> 
  select(
    land,
    land_new
  )

# Funktionen auf (neue und alte) Spalten anwenden

df_pisa_land_name_land_id |> 
  mutate(
    land_new = haven::as_factor(land)
  ) |> 
  select(
    land,
    land_new
  ) |>
  rename(
    land_labels = land_new
  ) |> 
  mutate(
    land_labels = as.character(land_labels),
    albanien = if_else(land == "ALB", TRUE, FALSE),
    anzahl = sum(albanien)
  ) 

# Vergleichsoperatoren, um Werte zu vergleichen:
#
# Ist gleich: ==
# Größer: >
# Größer und gleich: >=
# Kleiner: <
# Kleiner und gleich: <=
#
# Die Operatoren werden benötigt, um Vergleichsbedingungen zu formulieren.
# Wann soll etwas geschehen? Genauer: Wann tritt der Fall auf und wann nicht.

# Summieren von Spalten ----

df_pisa_land_name_land_id |> 
  mutate(
    land_new = haven::as_factor(land)
  ) |> 
  select(
    land,
    land_new
  ) |>
  rename(
    land_labels = land_new
  ) |>
  mutate(
    land_labels = as.character(land_labels),
    albanien = if_else(land == "ALB", 1, 0),
    deutschland = if_else(land == "DEU", 1, 0),
    anzahl_deutschland = sum(deutschland)
  ) |>
  summarise(
    anzahl = sum(albanien)
  )

# Drei Spalten summieren:

df_pisa_land_name_land_id |> 
  mutate(
    summe = CNTSCHID + CNTTCHID + TEACHERID
  )

# Zählen von Fällen ----

df_pisa_land_name_land_id |> 
  mutate(
    land_new = haven::as_factor(land)
  ) |> 
  select(
    land,
    land_new
  ) |>
  rename(
    land_labels = land_new
  ) |>
  count(land) |> 
  rename(
    "Anzahl" = n,
    "Land (PISA)" = land
  ) |> 
  haven::as_factor()

# Reihenfolge ändern ----

df_anzahl_tn_laender <- 
  df_pisa_land_name_land_id |> 
    mutate(
      land_new = haven::as_factor(land)
    ) |> 
    select(
      land,
      land_new
    ) |>
    rename(
      land_labels = land_new
    ) |>
    count(land) |> 
    haven::as_factor() |> 
    mutate(
      land = as.character(land)
    ) |>
    arrange(desc(n)) |>
    arrange(n) |>
    arrange(desc(land)) |> 
    arrange(land) |>
    rename(
      "Anzahl" = n,
      "Land (PISA)" = land
    )

df_anzahl_tn_laender

# Eine Spalte / Variablen exportieren (aus dem Tibble) ----

####### TODO: DAS SOLL SEHR, SEHR BALD IN EINEN TEXT (WORD) ########

# "Klassisch"
df_anzahl_tn_laender[1, 2]

# Alternative Möglichkeit

vektor_anzahl_tn_laender <- 
  df_anzahl_tn_laender |> 
    pull(Anzahl)

df_anzahl_tn_laender |> 
  pull(`Land (PISA)`)

vektor_anzahl_tn_laender[1]

# Auswahl von Fällen / Zeilen -----

df_anzahl_tn_laender

# Auswahl der ersten 5 Fälle

df_anzahl_tn_laender |> 
  slice(1:5)

# Auwahl spezifischer Fälle

df_anzahl_tn_laender |> 
  slice(c(1, 3, 5))

# Auswahl nach spezielle Kriterien
# vgl. Vergleichsoperatoren

df_anzahl_tn_laender |> 
  filter(Anzahl >= 4000) |> 
  filter(`Land (PISA)` == "Chile")

# Vergleichsbedingungen verknüpfen:
#
# Und: &
# Oder: |
#
# Weiterführende Hilfe:
?`|`

df_anzahl_tn_laender |> 
  filter(Anzahl >= 4000 | `Land (PISA)` == "Chile")

# R Objekte speichern ---------------------------------------------------------------------------------------------

write_rds(
  x = df_anzahl_tn_laender,
  file = "repository/df-anzahl-tn-laender.rds"
)
