# Erste Visualisierungsversuche meiner ausgewählten Daten

# Pakete laden ----------------------------------------------------------------------------------------------------

library(tidyverse)
library(ggthemes)

# Daten laden -----------------------------------------------------------------------------------------------------

df_pisa_auswahl <- read_rds("repository/df-pisa-auswahl.rds")
df_pisa_auswahl

# Daten aufbereiten für die Visualisierung ------------------------------------------------------------------------
df_pisa_plot <- 
  df_pisa_auswahl |> 
  mutate(
    gender_neu = if_else(gender_neu == "Gender: Female", "Frauen", "Männer")
  ) |> 
  select(gender_neu) |> 
  drop_na()
df_pisa_plot

# Visualisierung der Daten ----------------------------------------------------------------------------------------

ggplot()

p_pisa_gender <- 
  ggplot(
    # Den Link zum Datensatz (Tibble) herstellen:
    data = df_pisa_plot,
    # Den Link zur konkreten Spalte herstellen:
    mapping = aes(
      x = gender_neu
    )
  ) +
  # Geometrische Objekte zeichnen (Balken, Punkte, Linien …)
  geom_bar() #+
  #  geom_dotplot()

p_pisa_gender +
  theme_gdocs()

# Speichern des Plots als R Objekt --------------------------------------------------------------------------------

write_rds(
  x = p_pisa_gender,
  file = "repository/p-pisa-gender.rds",
  compress = "gz"
)

# Speichern des Plots als Bild ------------------------------------------------------------------------------------

ggsave("plots/Mein toller Pisa Plot.jpg")
