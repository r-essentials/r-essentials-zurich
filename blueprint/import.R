# Rohdaten aus SPSS-Datei importieren

# Pakete laden ----------------------------------------------------------------------------------------------------

library(tidyverse)
library(haven)

# Daten laden -----------------------------------------------------------------------------------------------------

df_pisa_spss_raw <- read_sav(file = "data/teacher-pisa-selection.sav")
df_pisa_spss_raw

# Daten aufbereiten -----------------------------------------------------------------------------------------------

df_pisa_spss_factors <- as_factor(df_pisa_spss_raw)

# Datein speichern ------------------------------------------------------------------------------------------------
# ?write_rds

write_rds(
  x = df_pisa_spss_factors,
  file = "repository/df-pisa-spss-factors.rds",
  compress = "gz"
)
